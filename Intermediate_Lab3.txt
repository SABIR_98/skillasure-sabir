import java.util.Scanner;
public class Main
{
	public static void main(String[] args) 
	{   
	    int number,remainder,temp,reverse=0;
	    
	    Scanner sc = new Scanner(System.in);
	    
	    //to print desired input
	    System.out.print("Enter a positive integer :");
	    number=sc.nextInt();
	    
	    temp=number;
	    
	    while(temp>0)
	    { 
	        //Logic parts
	        remainder=temp%10;
	        reverse=reverse*10+remainder;
	        temp=temp/10;
	    }
	    
	   //To print desored output
		System.out.println("Your reverse word is :"+reverse);
	}
}
